import 'package:flutter/material.dart';
import 'package:whatsapp_tutorial/src/login_form_screen.dart';

void main() {
  runApp(new WhatsappClone());
}

class WhatsappClone extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: "Whatsapp",
      theme: new ThemeData(
        primaryColor: Color.fromRGBO(7, 94, 84, 1.0)
      ),
      home: new LoginForm(),
    );
  }
}


