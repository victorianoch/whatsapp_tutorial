import 'package:flutter/material.dart';
import 'package:whatsapp_tutorial/src/helpers/validator.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: new Text('Whatsapp'),
        ),
        body: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: TextFormField(
                  controller: _usernameController,
                  decoration: InputDecoration(
                    hintText: 'example@thecodingpapa.com',
                    labelText: 'Email',
                  ),
                  validator: (value) {
                    print("email validate triggered!");
                    if (value.isEmpty || !isEmail(value)) {
                      return 'Please enter valid email';
                    }
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Container(
                  constraints: BoxConstraints.expand(
                    height: Theme.of(context).textTheme.display1.fontSize * 1.1,
                  ),
                  child: RaisedButton(
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        print('Email submitted for login!');
                      }
                    },
                    child: Text('Login'),
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
